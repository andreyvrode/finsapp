//
//  PeerBrowserViewController.swift
//  FinsApp
//
//  Created by Андрей on 24/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class PeerBrowserViewController: UIViewController {
    
    var personName: String!
    
    var myPeerID: MCPeerID!
    var session: MCSession!
    var advertizer: MCNearbyServiceAdvertiser!
    var browser: MCNearbyServiceBrowser!

    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        self.sendMessage(text: "lalalalalla")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.personName = "Andrey Andrey"
        
        self.myPeerID = MCPeerID(displayName: self.personName)
        self.session = MCSession(peer: self.myPeerID, securityIdentity: nil, encryptionPreference: .none)
        //self.session.encryptionPreference = MCEncryptionPreference.none
        //self.session.delegate = self
        
        let discoveryInfo = ["username" : "Andreyandreyyandreyyy"]
        let serviceType = "tinkoff-chat"
        
        self.advertizer = MCNearbyServiceAdvertiser(peer: self.myPeerID,
                                                   discoveryInfo: discoveryInfo,
                                                   serviceType: serviceType)
        self.advertizer.startAdvertisingPeer()
        
        self.browser = MCNearbyServiceBrowser(peer: self.myPeerID,
                                              serviceType: serviceType)
        self.browser.delegate = self
        
        self.browser.startBrowsingForPeers()
        
        
    }
    
    
    
    func sendInvite(to peer: MCPeerID) {
        print("[INVITE TO IGOR]")
        self.browser.invitePeer(peer, to: self.session, withContext: "tinkoff-chat".data(using: .utf8), timeout: 100.0)
    }
    func sendMessage(text: String){
        for peer in self.session.connectedPeers {
            let data = text.data(using: .utf8)
            do {
                try self.session.send(data!,
                                      toPeers: [peer],
                                      with: .reliable)
            } catch {
                print("[SEND ERROR] \(error.localizedDescription)")
            }
        }
        
        
    }

}

extension PeerBrowserViewController : MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        print("[FOUND PEER] Name: \(peerID.displayName)")
        if peerID.displayName == "Igor's iPhone" {
            if !self.session.connectedPeers.contains(peerID) {
                print("[Igor not connected, sending request]")
                self.sendInvite(to: peerID)
            }
            print("[SESSION CONNECTED]\(self.session.connectedPeers.count)")
            print("[Igor already connected, sending message]")
        }
        //self.sendMessage(text: "Prodam garazh llalalal")
    }
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("[LOST PEER] Name: \(peerID.displayName)")
    }
    
}
extension PeerBrowserViewController : MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        
    }
}

extension PeerBrowserViewController : MCSessionDelegate {

    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print("[PEER CHANGE STATE] \(peerID.displayName) to \(state.rawValue)")
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        let text = data as! String
        print("New Message")
        print("[RECV MESSAGE] text: \(data)")
    }
    
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    
    // Start receiving a resource from remote peer.
    @available(iOS 7.0, *)
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) { }
    
}

