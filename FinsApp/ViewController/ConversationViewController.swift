//
//  ConversationViewController.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: fromStoryBoard
extension ConversationViewController {
    static var stringID: String { return "ConversationVC" }
    static var fromStoryBoard : ProfileEditViewController {
        return UIStoryboard(name: "Conversations", bundle: Bundle.main).instantiateViewController(withIdentifier: self.stringID) as! ProfileEditViewController
    }
}
