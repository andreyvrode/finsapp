//
//  ProfileEditViewController.swift
//  FinsApp
//
//  Created by Андрей on 21/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit

typealias EmptyHandler  = () -> ()

class ProfileEditViewController: UIViewController {
    let account = Account.shared
    
    // MARK: Outlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var userInfoTextView: UITextView!

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var selectPhotoButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let imagePicker = UIImagePickerController()
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.finalizeView()
        self.loadProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.usernameTextField.becomeFirstResponder()
    }
    
    // MARK: Actions
    @IBAction func selectPhotoButtonPressed(_ sender: UIButton) {
        self.showImagePickDialog()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        self.updateProfile {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

// MARK: Private methods
extension ProfileEditViewController {
    fileprivate func finalizeView(){
        let textfieldPadding: UIView = UIView(frame: CGRect(x: 0, y: 0,
                                                            width: 10, height: 5))
        self.avatarImageView.clipsToBounds = true
        self.selectPhotoButton.imageView?.clipsToBounds = true
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2
        self.selectPhotoButton.layer.cornerRadius = self.selectPhotoButton.frame.size.height / 2
        
        self.userInfoTextView.layer.cornerRadius = 15
        self.usernameTextField.layer.cornerRadius = 7
        self.usernameTextField.leftView = textfieldPadding
        self.usernameTextField.leftViewMode = .always
        
        self.saveButton.layer.cornerRadius = 7
        self.cancelButton.layer.cornerRadius = 7
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
    }
    
    @objc func dismissKeyboard() {
        self.userInfoTextView.resignFirstResponder()
        self.usernameTextField.resignFirstResponder()
    }

}

// MARK: Data storing
extension ProfileEditViewController {
    fileprivate func loadProfile() {
        self.usernameTextField.text = self.account.userName
        self.userInfoTextView.text = self.account.userAbout
        self.avatarImageView.image = UIImage(data: self.account.userAvatar)
    }
    
    fileprivate func updateProfile(completion: EmptyHandler? = nil) {
        self.account.updateUserInfo(name: self.usernameTextField.text ?? "Unknown",
                                    about: self.userInfoTextView.text ?? "-",
                                    image: UIImagePNGRepresentation(self.avatarImageView.image!)!)
        completion?()
    }
}

// MARK: User photo manager
extension ProfileEditViewController: (UIImagePickerControllerDelegate&UINavigationControllerDelegate) {
    fileprivate func showImagePickDialog(){
        let alertController = UIAlertController(title: "Upload new image", message: "Select source",
                                                preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .default,
                                                handler: { action in
                                                    self.imagePicker.sourceType = .camera
                                                    self.present(self.imagePicker,
                                                                 animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "From library", style: .default,
                                                handler: { action in
                                                    self.imagePicker.sourceType = .photoLibrary
                                                    self.present(self.imagePicker,
                                                                 animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel,
                                                handler: nil))
        self.present(alertController,
                     animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let newImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.avatarImageView.image = newImage
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
}

// MARK: fromStoryBoard
extension ProfileEditViewController {
    static var stringID: String { return "ProfileEditVC" }
    static var fromStoryBoard : ProfileEditViewController {
        return UIStoryboard(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: self.stringID) as! ProfileEditViewController
    }
}
