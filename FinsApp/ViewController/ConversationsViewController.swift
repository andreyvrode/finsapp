//
//  ConversationsListViewController.swift
//  FinsApp
//
//  Created by Андрей on 07/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit


class ConversationsViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var convesationsListTableView: UITableView!
    
    // MARK: Property
    private let account = Account.shared
    var testConversationList: [TestConversation] = TestConversation.testList
    
    private var communicationService : CommunicationService!
    private let identifier = String(describing: ConversationListTableViewCell.self)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.finalizeInit()
        let convStorageProvider = ConversationStorageProvider()
        for i in 0...10 {
            let conversation = Conversation(with: String.generateID())
            convStorageProvider.save(convesation: conversation)
        }
        
        print("[Items generated]")
        
        let list = convStorageProvider.getConversationList()
//        self.communicationService = CommunicationService()
//        self.communicationService.peer = account.peer
//        self.communicationService.online = true
    }

    // MARK: Actions
    
}

extension ConversationsViewController {
    private func finalizeInit() {
        self.convesationsListTableView.dataSource = self
        self.convesationsListTableView.delegate   = self
        self.convesationsListTableView.register(UINib(nibName: self.identifier, bundle: nil),
                                                forCellReuseIdentifier: self.identifier)
    }
}

extension ConversationsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.testConversationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard var cell = self.convesationsListTableView.dequeueReusableCell(withIdentifier: self.identifier) as? ConversationCellConfiguration else {
//            let cell = self.convesationsListTableView.dequeueReusableCell(withIdentifier: "default")
//            cell?.textLabel?.text = "<Cell dequeue error>"
//            return cell!
//        }
//        //print("Conversation: \(self.testConversationList[indexPath.row])")
//        cell.name =  self.testConversationList[indexPath.row].name
//        cell.message = self.testConversationList[indexPath.row].message
//        cell.date = self.testConversationList[indexPath.row].date
//        cell.online = self.testConversationList[indexPath.row].online
//        cell.hasUnreadMessages = self.testConversationList[indexPath.row].hasUnreadMessages
//        return cell as! UITableViewCell
        return UITableViewCell()
    }
    
}

extension ConversationsViewController: UITableViewDelegate{
    
}

// MARK: fromStoryBoard
extension ConversationsViewController {
    static var stringID: String { return "ConversationsVC" }
    static var fromStoryBoard : ProfileEditViewController {
        return UIStoryboard(name: "Conversations", bundle: Bundle.main).instantiateViewController(withIdentifier: self.stringID) as! ProfileEditViewController
    }
}
