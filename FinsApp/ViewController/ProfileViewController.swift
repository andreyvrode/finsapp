//
//  ViewController.swift
//  FinsApp
//
//  Created by Андрей on 25/09/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userInfoLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var startSessionBarButtonItem: UIBarButtonItem!
    
    
    var account: Account = Account.shared
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.finalizeView()
        
        account.saveViaCoreData()
        account.loadViaCoreData()
    }
    
    deinit {
        print("[ProfileVC] deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateProfile()
    }
    
    // MARK: Actions
    @IBAction func editButtonPressed(_ sender: UIButton) {
        self.present(ProfileEditViewController.fromStoryBoard,
                     animated: true, completion: nil)
    }
    
    @IBAction func startSessionBarButtomItemPressed(_ sender: UIBarButtonItem) {
        let conversationsViewController = UIStoryboard(name: "Conversations", bundle: Bundle.main).instantiateInitialViewController() as! UINavigationController
        self.view.window?.rootViewController = conversationsViewController
    }
}


// MARK: Private methods
extension ProfileViewController {
    fileprivate func updateProfile() {
        self.usernameLabel.text = self.account.userName
        self.userInfoLabel.text = "\(self.account.userAbout) \n ID:\(self.account.peer.identifier)"
        self.avatarImageView.image = UIImage(data: self.account.userAvatar)
    }
    
    fileprivate func finalizeView(){
        self.avatarImageView.layer.cornerRadius = 40
        self.editButton.layer.borderWidth = 0
        self.editButton.layer.borderColor = UIColor.clear.cgColor
        self.editButton.layer.cornerRadius = 7
    }
}

// MARK: Helper
extension ProfileViewController {
    static var stringID: String { return "ProfileVC" }
    static var fromStoryBoard : ProfileViewController {
        return UIStoryboard(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: self.stringID) as! ProfileViewController
    }
}


