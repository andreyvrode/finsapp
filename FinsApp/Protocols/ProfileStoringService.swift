//
//  StorageService.swift
//  FinsApp
//
//  Created by Андрей on 04/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

protocol ProfileStoringService {
    // Write user profile to local storage
    func save(_ profile: User)
    
    // Read user profile from local storage
    func loadProfile(in account: Account)
}
