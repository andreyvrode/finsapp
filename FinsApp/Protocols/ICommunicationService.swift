//
//  ICommunicationService.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    // Service deligate
    var deligate: CommunicationServiceDeligate? { get set }
    
    // Online status
    var online: Bool { get set }
    
    // Send message
    func send(_ message: Message, to peer: Peer)
}
