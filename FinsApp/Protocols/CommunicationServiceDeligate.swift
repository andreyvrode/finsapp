//
//  CommunicationServiceDeligate.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

protocol CommunicationServiceDeligate: class {
    // Browsing
    func communicationService(_ communicationService: ICommunicationService,
                              didFoundPeer: Peer)
    func communicationService(_ communicationService: ICommunicationService,
                              didLostPeer: Peer)
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartBrowsingForPeers error: Error)
    
    // Advertising
    func communicationService(_ communicationService: ICommunicationService,
                              didRecieveInviteFromPeer peer: Peer,
                              invintationClosure: (Bool) -> ())
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartAdvertisingForPeers error: Error)
    
    // Messaging
    func communicationService(_ communicationService: ICommunicationService,
                              didRecieveMessage message: Message,
                              from peer: Peer)
    
}
