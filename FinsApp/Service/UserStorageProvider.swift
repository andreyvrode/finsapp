//
//  UserStorageProvider.swift
//  FinsApp
//
//  Created by Андрей on 04/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import CoreData

class UserStorageProvider : ProfileStoringService {
    
    func save(_ profile: User) {
        self.userPersistentContainer.performBackgroundTask { (context) in
            let userModel = NSEntityDescription.insertNewObject(forEntityName: self.entityName,
                                                                into: context) as? UserProfile

            userModel?.identifier = profile.identifier
            userModel?.name = profile.name
            userModel?.about = profile.about
            userModel?.image = NSData(data: profile.image!)
            try? context.save()
        }
    }
    
    func loadProfile(in account: Account) {

        self.userPersistentContainer.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: self.entityName)
            let userProfileEntry = try? context.fetch(fetchRequest)
            guard userProfileEntry?.count != 0 else {
                print("[UserStorageProvider] No user's in storage - \(userProfileEntry?.count ?? 0)")
                return
            }
            let user = User(identifier: userProfileEntry!.last!.identifier!,
                            name: userProfileEntry!.last!.name!,
                            about: userProfileEntry!.last!.about!,
                            image: userProfileEntry!.last!.image.unsafelyUnwrapped as Data)
            account.setUser(user)
        }
        
    }
    
    fileprivate lazy var userPersistentContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: "User")
        container.loadPersistentStores { (_, error) in
            guard error == nil
                else { print("[UserStorageProvider] Storage error: \(error?.localizedDescription ?? "-")"); return }
        }
        print("[UserStorageProvider] User persistent pontainer ready")
        return container
    }()
    fileprivate let entityName : String = "UserProfile"
    
}
