//
//  TestConversationService.swift
//  FinsApp
//
//  Created by Андрей on 11/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import CoreData

// Создать, список ID чатов, загрузить чат по ID
class ConversationStorageProvider {
    
    func save(convesation: Conversation) {
        self.userPersistentContainer.performBackgroundTask { (context) in
            let storageModel = NSEntityDescription.insertNewObject(forEntityName: "ConversationModel",
                                                                   into: context) as! ConversationModel
            storageModel.identifier = convesation.identifier
            storageModel.targetPeerID = convesation.targetPeerID
            
            try? context.save()
            print("[ConversationStorageProvider] New conv saved")
        }
    }
    
    func getConversationList() -> [String]? {
        self.userPersistentContainer.performBackgroundTask { (context) in
            guard let list = try? context.fetch(ConversationModel.fetchRequest())
                else { print("[ConversationStorageProvider] getConversationList - failed"); return}
            guard list.count != 0
                else { print("[ConversationStorageProvider] getConversationList - empty"); return}
            print("[ConversationStorageProvider] getConversationList - \(list.count) items fetched")
        }
        return nil
    }
    
    func conversation(with id: String) -> () {
        
    }
    
    fileprivate lazy var userPersistentContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: "User")
        container.loadPersistentStores { (_, error) in
            guard error == nil
                else { print("[ConversationStorageProvider] Storage error: \(error?.localizedDescription ?? "-")"); return }
        }
        print("[ConversationStorageProvider] User persistent pontainer ready")
        return container
    }()
}
