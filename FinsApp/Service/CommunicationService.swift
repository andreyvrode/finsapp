//
//  CommunicationService.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CommunicationService: NSObject, ICommunicationService {
    weak var deligate : CommunicationServiceDeligate?
    var online: Bool {
        set {
            switch newValue {
            case true:
                self.setOnline()
                self.isOnline = true
                print("[CommunicationService] Now in \(self.isOnline)")
            case false:
                self.setOnline()
                self.isOnline = false
                print("[CommunicationService] Now in \(self.isOnline)")
            }
        }
        get { return self.isOnline}
    }
    
    private var isOnline: Bool = false
    public var peer: Peer! {
        didSet {
            print("[CommunicationService] peer did set to \(self.peer.name)")
            self.myPeerID = MCPeerID(displayName: self.peer.name)
        }
    }
    private var myPeerID: MCPeerID! {
        didSet {
            guard self.myPeerID != nil else {
                print("[CommunicationService] ERROR: myPeerID is nil")
                return
            }
            print("[CommunicationService] myPeerID did set to \(self.myPeerID.displayName)")
            self.session = MCSession(peer: self.myPeerID,
                                     securityIdentity: nil,
                                     encryptionPreference: .none)
            self.session.delegate = self
        }
    }
    private var session: MCSession! {
        didSet {
            print("[CommunicationService] session did set with peerID - \(self.session.myPeerID)")
            self.advertizer = MCNearbyServiceAdvertiser(peer: self.myPeerID,
                                                        discoveryInfo: self.discoveryInfo,
                                                        serviceType: self.serviceType)
            self.advertizer.delegate = self
        }
    }
    private var advertizer: MCNearbyServiceAdvertiser! {
        didSet {
            print("[CommunicationService] advertizer did set with peerID - \(self.advertizer.myPeerID.displayName), serviceType: \(self.advertizer.serviceType), discoveryInfo: \(self.advertizer.discoveryInfo?.description ?? "-")")
            self.browser = MCNearbyServiceBrowser(peer: self.myPeerID,
                                                  serviceType: self.serviceType)
            self.browser.delegate = self
        }
    }
    private var browser: MCNearbyServiceBrowser! {
        didSet {
            print("[CommunicationService] browser did set with peerID - \(self.browser.myPeerID.displayName), serviceType: \(self.browser.serviceType)")
        }
    }
    private var serviceType: String = "tinkoff-chat"
    private var discoveryInfo: [String : String]{
        return ["userName" : self.peer.name]
    }
    
    override init() {
        super.init()
        print("[CommunicationService] init")
    }
    
    func send(_ message: Message, to peer: Peer) {
        print("[CommunicationService] Sending message to \(peer.name)")
    }
    
    // MARK: Private methods
    private func setOnline(){
        self.advertizer.startAdvertisingPeer()
        self.browser.startBrowsingForPeers()
    }
    private func setOffline(){
        self.advertizer.stopAdvertisingPeer()
        self.browser.stopBrowsingForPeers()
    }
}

// Advertising
extension CommunicationService : MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID,
                    withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        
        print("[Advertiser] Got invite from \(peerID.displayName) context: \(context?.debugDescription ?? "-")")
        invitationHandler(true, self.session)
//        self.deligate?.communicationService(self, didRecieveInviteFromPeer: Peer(identifier: "test", name: peerID.displayName), invintationClosure: {(status) in
//
//        })
    }
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print("[Advertiser] didNotStartAdvertisingPeer with error \(error.localizedDescription)")
        self.deligate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }
}
// Browsing
extension CommunicationService : MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        print("[Browser] foundPeer: \(peerID.displayName), discoveryInfo: \(info.debugDescription)")
        self.browser.invitePeer(peerID, to: self.session,
                                withContext: self.serviceType.data(using: .utf8),
                                timeout: 30.0)
        self.deligate?.communicationService(self, didFoundPeer: Peer(identifier: "test", name: peerID.displayName))
    }
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("[Browser] lostPeer: \(peerID.displayName)")
        self.deligate?.communicationService(self, didLostPeer: Peer(identifier: "test", name: peerID.displayName))
    }
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("[Browser] didNotStartBrowsingForPeers with error \(error.localizedDescription)")
    }
}
// Session
extension CommunicationService : MCSessionDelegate {
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print("[Session] didReceive data from \(peerID.displayName)")
        // FIXME: make safe unwrap
        let message = try! JSONDecoder().decode(Message.self, from: data)
        self.deligate?.communicationService(self, didRecieveMessage: message, from: Peer(identifier: "-", name: peerID.displayName))
    }
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .notConnected:
            print("[Session] \(peerID.displayName) - Not connected!")
        case .connecting:
            print("[Session] \(peerID.displayName) - Connecting..")
        case .connected:
            print("[Session] \(peerID.displayName) - Connected")
            let message = Message(identifier: String.generateID(),
                                  text: "Hi \(peerID.displayName), i'm \(self.peer.name). Welcome to \(browser.serviceType)!")
            let json = try! JSONEncoder().encode(message)
            try! self.session.send(json,
                              toPeers: [peerID], with: .reliable)
        }
        
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) { }
}
