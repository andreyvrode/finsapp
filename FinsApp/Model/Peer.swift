//
//  Peer.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

struct Peer {
    let identifier : String
    let name : String
}
