//
//  Person.swift
//  FinsApp
//
//  Created by Андрей on 21/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import UIKit

struct User {
    let identifier : String 
    let name : String
    let about : String
    let image: Data?
    
    init(identifier: String, name: String, about: String, image: Data?) {
        self.identifier = identifier
        self.about = about
        self.name = name
        self.image = image
    }

}
