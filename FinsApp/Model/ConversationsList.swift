//
//  ConversationsList.swift
//  FinsApp
//
//  Created by Андрей on 07/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

struct TestConversation {
    var name : String?
    var message: String?
    var date : Date?
    var online : Bool
    var hasUnreadMessages : Bool
}

extension TestConversation {
    static var testList : [TestConversation] {
        let testConversationList: [TestConversation] = [TestConversation(name: "Alex Testov",
                                                                 message: "Cras dapibus in tellus eget ornare.",
                                                                 date: Date(),
                                                                 online: true,
                                                                 hasUnreadMessages: true),
                                                    TestConversation(name: "Mihail Cheburekov",
                                                                 message: "Proin porttitor laoreet sapien, a lobortis sem elementum placerat. Morbi sodales, massa bibendum viverra fermentum, massa purus imperdiet lectus, in scelerisque dui sem convallis nisl.",
                                                                 date: Date(),
                                                                 online: true,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Pavel Tehnik",
                                                                 message: "Mauris quis euismod magna. Morbi ut ipsum neque.",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: true),
                                                    TestConversation(name: "Vovan Britva",
                                                                 message: "Donec varius, ante non luctus commodo, tellus metus dictum leo, nec venenatis diam tortor in urna. In hac habitasse platea dictumst. Fusce at diam id ipsum convallis vehicula",
                                                                 date: Date(),
                                                                 online: true,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Masha Petrova",
                                                                 message: "Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33 Цицерона и их английский перевод, сделанный H. Rackham, 1914 год",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Oleg Lerner",
                                                                 message: "Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Adam Eleedov",
                                                                 message: "Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney",
                                                                 date: Date(),
                                                                 online: true,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Svetlana Semenova",
                                                                 message: "Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Nikolai Bascov",
                                                                 message: "Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: false),
                                                    TestConversation(name: "Super Man",
                                                                 message: "Уже лечу",
                                                                 date: Date(),
                                                                 online: false,
                                                                 hasUnreadMessages: false)
        ]
        return testConversationList
    }
}
