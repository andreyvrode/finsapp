//
//  Account.swift
//  FinsApp
//
//  Created by Андрей on 29/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import UIKit

class Account {
    static let shared = Account()
    
    private var userid: String!
    
    private var user: User! {
        didSet {
            print("[Account] user didSet")
            DispatchQueue.global().async {
                self.save()
            }
            
        }
    }
    var comminucationService: CommunicationService!
    
    private init() {
        if let identifier = UserDefaults.standard.string(forKey: "Account.User.Identifier") {
            self.load()
        } else {
            self.userid = String.generateID()
            self.user = User(identifier: self.userid,
                             name: userNameDefault,
                             about: userAboutDefault,
                             image: userAvatarDefault)
        }
    }
    
    var userName : String {
        return self.user.name
    }
    var userAbout : String {
        return self.user.about
    }
    var userAvatar : Data {
        return self.user.image!
    }
    
    var userNameDefault : String {
        return UIDevice.current.name
    }
    var userAboutDefault : String {
        return "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)"
    }
    var userAvatarDefault : Data {
        return UIImagePNGRepresentation(UIImage(named: "placeholder-user")!)!
    }
    
    func updateUserInfo(name: String, about: String, image: Data) {
        self.user = User(identifier: self.userid,
                         name: name.isEmpty ? userNameDefault : name,
                         about: about.isEmpty ? userAboutDefault : about,
                         image: image)
    }
}

// Peer
extension Account {
    var peer: Peer{
        return Peer(identifier: self.user.identifier, name: self.user.name)
    }
}

// DataStore
extension Account {
    func load() {
        let storage = UserDefaults.standard
        guard   let identifier = storage.string(forKey: "Account.User.Identifier"),
                let name = storage.string(forKey: "Account.User.Name"),
                let about = storage.string(forKey: "Account.User.About"),
                let image = storage.data(forKey: "Account.User.Avatar")
        else { print("[Account] load from storage error"); return }
        
        self.userid = identifier
        self.user = User(identifier: self.userid,
                         name: name,
                         about: about,
                         image: image)
        print("[Account] loaded from storage")
    }
    
    func save() {
        let storage = UserDefaults.standard
        
        storage.setValue(self.userid, forKey: "Account.User.Identifier")
        storage.setValue(self.user.name, forKey: "Account.User.Name")
        storage.setValue(self.user.about, forKey: "Account.User.About")
        storage.setValue(self.user.image, forKey: "Account.User.Avatar")
        storage.synchronize()
        print("[Account] saved to storage")
    }
}

// DataStore
extension Account {
    func loadViaCoreData() {
        let storageService = UserStorageProvider() as ProfileStoringService
        storageService.loadProfile(in: self)
    }
    
    func saveViaCoreData() {
        let storageService = UserStorageProvider() as ProfileStoringService
        
        storageService.save(self.user)
        print("[Account] User profile saved via CoreData")
    }
    
    func setUser(_ user: User) {
        self.user = user
        print("[Account] User was set")
    }
}
