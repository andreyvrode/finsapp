//
//  Conversation.swift
//  FinsApp
//
//  Created by Андрей on 11/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

class Conversation {
    var identifier: String
    var targetPeerID: String
    
    private init(identifier: String, targetPeerID: String) {
        self.identifier = identifier
        self.targetPeerID = targetPeerID
    }
    
    convenience init(with peer: String) {
        self.init(identifier: String.generateID(),
                  targetPeerID: peer)
    }
}
