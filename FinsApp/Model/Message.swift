//
//  Message.swift
//  FinsApp
//
//  Created by Андрей on 28/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

struct Message: Codable {
    enum `Type` : String {
        case textMessage = "TextMessage"
    }
    let identifier : String
    let text : String
    
}

/*
 @NSManaged public var conversationID: String?
 @NSManaged public var identifier: String?
 @NSManaged public var isUnread: Bool
 @NSManaged public var senderID: String?
 @NSManaged public var text: String?
 @NSManaged public var timestamp: NSDate?
 @NSManaged public var relatedConversation: ConversationModel?
 */
