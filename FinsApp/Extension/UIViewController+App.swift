//
//  UIViewController+App.swift
//  FinsApp
//
//  Created by Андрей on 29/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
