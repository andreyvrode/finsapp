//
//  ConversationModel+CoreDataProperties.swift
//  FinsApp
//
//  Created by Андрей on 11/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//
//

import Foundation
import CoreData


extension ConversationModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConversationModel> {
        return NSFetchRequest<ConversationModel>(entityName: "ConversationModel")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var targetPeerID: String?

}
