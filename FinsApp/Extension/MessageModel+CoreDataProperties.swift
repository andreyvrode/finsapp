//
//  MessageModel+CoreDataProperties.swift
//  FinsApp
//
//  Created by Андрей on 11/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//
//

import Foundation
import CoreData


extension MessageModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MessageModel> {
        return NSFetchRequest<MessageModel>(entityName: "MessageModel")
    }

    @NSManaged public var conversationID: String?
    @NSManaged public var identifier: String?
    @NSManaged public var isUnread: Bool
    @NSManaged public var senderID: String?
    @NSManaged public var text: String?
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var relatedConversation: ConversationModel?

}
