//
//  UserProfile+CoreDataProperties.swift
//  FinsApp
//
//  Created by Андрей on 04/11/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//
//

import Foundation
import CoreData


extension UserProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfile> {
        return NSFetchRequest<UserProfile>(entityName: "UserProfile")
    }

    @NSManaged public var about: String?
    @NSManaged public var identifier: String?
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?

}
