//
//  DateFormatter+App.swift
//  FinsApp
//
//  Created by Андрей on 29/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    enum Format: String {
        case HHmm           = "HH:mm"
        case ddMMyyyy       = "dd-MM-yyyy"
        case yyyyMMdd       = "yyyy-MM-dd"
        case yyyyMMddHHmm   = "yyyy-MM-dd'T'HH:mm"
        case yyyyMMddHHmmss = "yyyy-MM-dd'T'HH:mm:ss"
    }
    
    static func formatter(by stringFormat: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = stringFormat
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale = Locale(identifier: "ru_RU")
        return formatter
    }
    
    static func formatter(_ format: Format) -> DateFormatter {
        return formatter(by: format.rawValue)
    }
}
