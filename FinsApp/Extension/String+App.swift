//
//  String+App.swift
//  FinsApp
//
//  Created by Андрей on 29/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import Foundation

extension String {
    static func generateID() -> String{
        let id = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return id!
    }
}
