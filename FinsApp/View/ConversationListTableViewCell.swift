//
//  ConversationListTableViewCell.swift
//  FinsApp
//
//  Created by Андрей on 07/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit

protocol ConversationCellConfiguration {
    var name : String? {get set}
    var message: String? {get set}
    var date : Date? {get set}
    var online : Bool { get set}
    var hasUnreadMessages : Bool {get set}
}

class ConversationListTableViewCell: UITableViewCell, ConversationCellConfiguration {
    var name: String?{
        set{
            self.usernameLabel.text = newValue
        }
        get{
            return self.usernameLabel.text
        }
    }
    
    var message: String?{
        set{
            self.messagePreviewLabel.text = newValue
        }
        get{
            return self.messagePreviewLabel.text
        }
    }
    
    var date: Date?{
        set{
            let df = DateFormatter.formatter(.HHmm)
            self.dateTimeLabel.text = df.string(from: newValue ?? Date())
        }
        get{
            return Date()
        }
    }
    
    var online: Bool{
        set{
            self.isOnlineView.isHidden = !newValue
        }
        get{
            return self.isOnlineView.isHidden
        }
    }
    
    var hasUnreadMessages: Bool{
        set{
            self.isUnreadMessageView.isHidden = !newValue
        }
        get{
            return self.isUnreadMessageView.isHidden
        }
    }
    
    
    // MARK: Outlets
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var messagePreviewLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var isOnlineView: UIView!
    @IBOutlet weak var isUnreadMessageView: UIView!
    
    
    fileprivate func finalizeUI() {
        self.avatarImageView.clipsToBounds = true
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.height / 2
        self.isOnlineView.clipsToBounds = true
        self.isOnlineView.layer.cornerRadius = self.isOnlineView.frame.width / 5
        
        self.isUnreadMessageView.clipsToBounds = true
        self.isUnreadMessageView.layer.cornerRadius = self.isUnreadMessageView.frame.height / 2
        
        self.isOnlineView.isHidden = true
        self.isUnreadMessageView.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        finalizeUI()

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
