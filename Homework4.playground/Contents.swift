//: Playground - noun: a place where people can play

import UIKit

class Company{
    // MARK: Propery
    
    var name : String
    var ceo : CEO?
    
    // MARK: Initialization
    init(_ name: String) {
        self.name = name
        print("+[COMPANY] \(self.name) instance init")
    }
    deinit {
        print("-[COMPANY] \(self.name) instance deinit")
    }

}

class CEO{
    // MARK: Propery
    
    var name : String
    weak var company: Company?
    var productManager: ProductManager?
    

    // MARK: Initialization
    init(name: String, company: Company) {
        self.name = name
        self.company = company
        
        print("+[CEO] \(self.name) instance init")
    }
    deinit {
        self.onLeaving()
    }
    
    // MARK: Methods
    func salaryIncraseRewiew(from developer: Developer, up to: Double){
        if (developer.salary < 90000){
            developer.productManager?.incraseSalary(for: developer, up: to)
            print("[BUH] Request from \(developer.name) approved")
        }
    }
    
    // Closures
    var companyInfo: (ProductManager?)->() = { (manager) in
        print(manager?.ceo.company?.description ?? "Company: nil")
    }
    
    var productManagerInfo: (ProductManager?)->() = { (manager) in
        print(manager?.description ?? "Manager: nil")
    }
    var developTeamInfo: (ProductManager?)->() = { (manager) in
        print(manager?.teamDescription ?? "ManageR: NIL")
    }
    
    func onLeaving(){
        print("-[CEO] \(self.name) instance deinit")
    }
}

class ProductManager{
    // MARK: Propery
    
    var name : String
    weak var ceo : CEO!
    
    var productName: String?
    var productSpec: String?
    
    var developers: [Developer?] = [Developer?]()
    var unassignedTasksQueue: [String] = [String](){
        didSet{
            if (self.unassignedTasksQueue.isEmpty){
                print("[PRODCUCT MANAGER] All task in <\(self.name)> is complete")
            }
        }
    }
    
    
    // MARK: Initialization
    init(name: String) {
        self.name = name
        print("+[PRODCUCT MANAGER] \(self.name) instance init")
    }
    deinit {
        self.onLeaving()
    }
    
    // MARK: Methods
    func perform(_ completion:@escaping (ProductManager?) -> ()){
        completion(self)
    }
    func addDeveloper(name: String){
        self.developers.append(Developer(manager: self, name: name, salary: 60000))
    }
    func removeDeveloper(at index: Int){
        self.developers.remove(at: index)
    }
    
    func taskFor(developer: Developer) -> String?{
        return self.unassignedTasksQueue.popLast()
    }
    
    func incraseSalary(for developer:Developer, up amount:Double){
        developer.salary += amount
        print("[PRODCUCT MANAGER]<\(self.name)> incraseSalary for \(developer.name)")
    }
    func onLeaving(){
        print("-[PRODCUCT MANAGER] \(self.name) instance deinit")
    }
    
}



class Developer{
    
    // MARK: Propery
    
    let name : String
    var salary: Double{
        didSet{
            // self.sayAllDevelopers("\(self.name) got salary incrase")
        }
    }
    
    weak var productManager : ProductManager?
    
    
    // MARK: Initialization
    init(manager: ProductManager, name: String, salary: Double) {
        self.name = name
        self.productManager = manager
        self.salary = salary
        
        print("+[DEVELOPER] \(self.name) instance init. By: \(self.productManager?.name ?? "<nil>")")


    }
    deinit {
        self.onLeaving()
    }
    

    // MARK: Methods
    
    func updateProductSpec(){
       print("[DEVELOPER] \(self.name) Just read product specification")
    }
    func askForNewTask(){
        if let task = self.productManager?.taskFor(developer: self){
            print("[DEVELOPER] Got new task for <\(self.name)> .... |- \(task) complete!")
        }else{
            print("[DEVELOPER] No tasks for <\(self.name)> .... !")
        }
    }
    
    func sayAllDevelopers(_ text: String){
        self.productManager?.developers.compactMap { [weak self] in
            guard let weakSelf = self else { return }
            $0?.message(from: weakSelf, with: text) }
    }
    
    func say(to developer:Developer, _ text: String){
        developer.message(from: self, with: text)
    }
    
    func message(from developer:Developer, with text: String){
        print("[NEW MESSAGE] to-> \(self.name)     FROM: <\(developer.name)>      |- TEXT: \(text)\n")
    }
    
    func makeSalaryIncraseRequestUp(to: Double){
        self.productManager?.ceo?.salaryIncraseRewiew(from: self, up: to)
        
    }
    
    func onLeaving(){
        print("-[DEVELOPER] \(self.name) instance deinit")
    }
    
}

// MARK: Extensions for CustomStringConvertible
extension Developer: CustomStringConvertible{
    var description: String {
        return """
    [DEVELOPER]
               |- Name: \(self.name)
               |- Manager: \(self.productManager?.name ?? "<nil>" ) with salary: \(salary)
               |- Product: \(self.productManager?.productName ?? "<nil>")
"""
    }
    
}

extension ProductManager: CustomStringConvertible{
    var description: String{
        return """
    [PRODUCT MANAGER]
                     |- Name: \(self.name)
                     |- Company: \(self.ceo?.company?.name ?? "<empty>")  CEO:\(self.ceo?.name ?? "<empty>")
                     |- Product: \(self.productName ?? "<empty>")  SPEC.: \(self.productSpec ?? "<empty>")
                     |- Team: \(self.developers.count) developers. \(self.unassignedTasksQueue.count) task remain
"""
    }
    
    var teamDescription: String{
        guard self.developers.count > 0 else {
            return """
[TEAM] - empty
"""
        }
        var developerReportList : [String] =  [String]()
        for d in self.developers{
            developerReportList.append(d?.description ?? "<nil>")
        }
        return """
[TEAM]
        
\(developerReportList.joined(separator: "\n"))
"""
    }
}

extension CEO: CustomStringConvertible{
    var description: String{
        return """
[CEO]
        |--Name: \(self.name),  Company:\(self.company?.name ?? "nil")
        |--Product manager: \(self.productManager?.name ?? "nil")
"""
    }
}

extension Company: CustomStringConvertible{
    var description: String{
        return """
        
        
[COMPANY] -- \(self.name),  CEO:\(self.ceo?.name ?? "nil")
"""
    }
}


// MARK: Company instance print
func aboutCompany(_ company: Company){
    
    print(company.description)
    print("------------------------------------------------------------")
    print(company.ceo?.description ?? "[ceo] is nil", "\n")
    print(company.ceo?.productManager?.description ?? "[productManager] is nil", "\n")
    print(company.ceo?.productManager?.teamDescription ?? "[productManager] is nil", "\n")
    print("=============================================================\n\n")
}

// MARK: Return test instance
/*
 productTaskCount: int - количество задач, которых надо выполнить программистам для завершения проекта
 teamSize : int - количество программистов
 */
func createTestCompany(productTaskCount: Int = 115, teamSize: Int = 10) -> Company{
    
    // Create Company
    var myCompany: Company = Company("Roga&Co")
    
    // Create CEO
    myCompany.ceo = CEO(name: "Alex Pupov", company: myCompany)
    myCompany.ceo?.productManager = ProductManager(name: "Ivan Petrov")
    myCompany.ceo?.productManager?.ceo = myCompany.ceo
    
    // Product set
    myCompany.ceo?.productManager?.productName = "Azino 777"
    myCompany.ceo?.productManager?.productSpec = "All tech documentation"
    
    // Create task queue for developers
    for i in 1...productTaskCount{
        myCompany.ceo?.productManager?.unassignedTasksQueue.append("Task #\(i)")
    }
    
    // Create develop team
    for i in 1...teamSize{
        myCompany.ceo?.productManager?.addDeveloper(name: "Dev Guy №\(i)")
    }
    
    // Show new instance
    print("\n\nCompany created:\n")
    aboutCompany(myCompany)
    
    
    // Closure testing
    print("<***************************************************************")
    
    if myCompany.ceo != nil{
        // Company
        myCompany.ceo?.productManager?.perform((myCompany.ceo?.companyInfo)!)
        
        //Manager
        myCompany.ceo?.productManager?.perform((myCompany.ceo?.productManagerInfo)!)
        
        //Team
        myCompany.ceo?.productManager?.perform((myCompany.ceo?.developTeamInfo)!)
    }
    
    print("***************************************************************>")
    //  Developer communication test #1 - Все посылают друг другу по сообщению
    //
    if let team = myCompany.ceo?.productManager?.developers{
         print("\n\n[Developer communication test #1] Все посылают друг другу по сообщению....\n")
        team.compactMap { guard let member = $0 else { return }; member.sayAllDevelopers("Hi, my name is \($0?.name ?? "nil")")}
    }
    
    //  Developer communication test #2 - 1 по списку просит повысить ЗП, запрос к СЕО
    //
    if let team = myCompany.ceo?.productManager?.developers{
        if team.count >= 1{
            if let first: Developer = team.first!{
                print("\n\n[Developer communication test #2] Первый по списку просит повысить ЗП, запрос к СЕО...\n")
                print(first.description)
                first.makeSalaryIncraseRequestUp(to: 120000) // Поднять до 120000
            }
        }
    }
    
    // Developers team work test #1 - Все берут по одному заданию, один круг
    //
    if let team = myCompany.ceo?.productManager?.developers{
        print("\n\n[Developer communication test #3] Все берут по одному заданию, один круг....\n")
        team.compactMap { guard let member = $0 else { return }; member.askForNewTask()}
    }
    
    
    // Show current instance
    print("\n\nCompany at current state:\n")
    aboutCompany(myCompany)
    
    
    
    //  Developers team work test #2 Разбирается очередь задач
    //
    if let team = myCompany.ceo?.productManager?.developers{
        if let manager = myCompany.ceo?.productManager{
            print("\n\n[Developer communication test #4] Разбирается очередь задач .... \n")
            while !manager.unassignedTasksQueue.isEmpty {
                team.compactMap { $0?.askForNewTask() }
            }
        }
    }
    
    
    //  Leak check
    //
    
//    // #1
//    print("\n\nCompany will close:.... \n")
//    aboutCompany(myCompany)
//    myCompany.ceo?.productManager?.developers.removeAll()
//
    
//    // #2
//    print("\n\nCompany without Devteam:.... \n")
//    aboutCompany(myCompany)
//
    
//    // #3
//    myCompany.ceo?.productManager = nil
//    print("\n\nCompany without productManager:.... \n")
//    aboutCompany(myCompany)
//
    
//    // #4
//    myCompany.ceo = nil
//    print("\n\nCompany without CEO:.... \n")
//    aboutCompany(myCompany)
    
    // #5
    myCompany = Company("Temp")
    print("\n\nCompany release:.... \n")
    aboutCompany(myCompany)
      return myCompany
}






// MARK: TEST INIT

 //Company
var myCompany = createTestCompany(productTaskCount: 10, teamSize: 3)











